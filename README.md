
파일 하나에 하나의 모듈 
자바스크립트 파일 하나에 하나의 모듈만 정의되어야 한다. 

define() 안의 상대적 모듈 이름들

define() 함수 내에서 일어날 수 있는 rquire("./relative/name") 호출에 대해서, 
의존성으로서 "require"을 요청했는지 확실히 해라, 
그렇게 해야 상대적 이름이 정확히 해결된다. 



define(["require", "./relative/name"], function(require) {
    var mod = require("./relative/name");
});



상대적 모듈 이름들은 다른 이름들과 상대적이다, 경로가 아니다.  

loader는 그것들의 이름으로 모듀들(modules)을 저장한다. 
그래서 상대적 이름 참조의 경우, 그것들은 레퍼런스를 만드는
모듈 이름에 상대적으로 해석된다. 
그런다음 모듈의 이름, 또는 아이디, 는 로드될 필요가 있을 경우에 경로로 변환된다. 


그것 안에 main, extras 모듈들이 있는 compute 패키지의 예제 

* lib/
    * compute/
        * main.js
        * extras.js


거기에 main.js 모듈은 이와 같을 것이다. 

define(["./extras"], function(extras) {
    //Uses extras in here.
});


이것이 경로 구성인 경우

require.config({
    baseUrl: 'lib',
    paths: {
      'compute': 'compute/main'
    }
});

require(['compute'])  이 완료된 다음에

lib/compute/main.js 'compute' 모듈 이름을 가질 것이다. 

그것이 './extras'를 요구할 때, 
그것은 'compute'의 상대적으로 해석된다. 
그래서 'compute/./extras', 
그것은 단지 extras로 normalizes 된다.



이게 맞나




