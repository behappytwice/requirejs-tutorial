/**
 * require는 알아서 주입됨 
 */
define(["require", "./lesson02sub"] , function(require, lesson02sub) {

    //console.log(require);

    return {
        userName : "홍길동",
        userAge : 30 ,
        lesson02sub : lesson02sub
    }
    
});