/**
 * require는 알아서 주입됨 
 */
define(["require"] , function(require) {

    /**
     * @see => https://github.com/requirejs/requirejs/issues/173
     * 
     * You can use require([]) to dynamically load things inside a define() call.
     * But note that require([]) is async, so it will call the function callback 
     * in a later turn, the loader will not call the callback function before the define()
     * factory function completes execution.

     define(['require', 'jquery'], function (require, jquery) {
        require(['something/else'], function (somethingElse) {

         });
     });
     */
    var isub; 
    require(["./sub"], function(sub) {
        console.log("sub====" + sub.deptName);
        console.log(sub);
        isub = sub;
    });
    // TO-DO : 방법을 찾아야 함
    return {
        userName : "홍길동",
        userAge : 30 ,
        sub: isub  // 외부에서 이 속성을 참조하면 오류 발생, 아직 isub가 로드되지 않은 상태에서 참조하기 때문에
    }
});