requirejs.config({
   
    // 디폴트로 js/lib으로부터 아무 모듈이나 로드한다.
    baseUrl: '/js/lib',

    // 모듈 ID가 app로 시작하면 js/app 디렉토리로부터 그것을 읽는다 
    // paths config는 baseUrl에 상대적이다
    // 그리고  절대 ".js"를 포함하지 않는다.
    // 왜냐하면 paths config는 directory를 위해 있을 수 있기 때문이다.
    paths: {
        app: '../app'
    }
});


/*
requirejs(['jquery'],
    function($) {
        var Vue = requirejs('vue');
        // requirejs()는 비도동기적으로 동작하기 때문에 
        // 아래 new Vue()는 오류 발생 
        // lesson13 처럼 function()을 사용해야 함
        new Vue( {
            el : '#app',
            template : `
               <h2>hello world</h2>
            `
        });

    }
);
*/




requirejs(['jquery', 'vue'],
    function($, Vue) {
        new Vue( {
            el : '#app',
            template : `
               <h2>hello world</h2>
            `
        });

    }
);


