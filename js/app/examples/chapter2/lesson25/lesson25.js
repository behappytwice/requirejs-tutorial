

/**
 * 상대경로 사용 
 */
define(["require", "vue", "./lesson25sub"] , function(require, Vue, lsub) {

    return  {
        template : `
           <div>
               <h2>Lesson25</h2>   
               <h2>after sub </h2>
               <lsub></lsub>
           </div>
        `,
        created() {
            console.log("created");
        },
        components : {        
            'lsub' : lsub
        } 
    };
});