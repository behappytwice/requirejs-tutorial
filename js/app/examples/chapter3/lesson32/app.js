requirejs.config({
   
    // 디폴트로 js/lib으로부터 아무 모듈이나 로드한다.
    baseUrl: '/js/lib',

    // 모듈 ID가 app로 시작하면 js/app 디렉토리로부터 그것을 읽는다 
    // paths config는 baseUrl에 상대적이다
    // 그리고  절대 ".js"를 포함하지 않는다.
    // 왜냐하면 paths config는 directory를 위해 있을 수 있기 때문이다.
    paths: {
        app: '../app',
        'Vue' : 'vue',
        'vue' : 'requirejs-vue'
    }
});


//https://embed.plnkr.co/plunk/Y2cEa3
requirejs( 
    ['Vue', 'vue'], function(Vue, vue) {
    
    requirejs(['vue!app/examples/chapter3/lesson32/lesson32'], function(lesson) {
        lesson.$mount('#app');
    });
    
});

