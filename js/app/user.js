
//  모듈이 의존성이 있으면 첫번째 인자는 모듈의 이름 배열이어야 한다.
//  두번째 인자는 정의 함수이어야 하는데 함수의 인자는 모듈의 이름을
//  순서대로 정의한다.
//  이 함수는 모듈이 모두 로드될 때까지 실행되지 않는다.
define(["./department"], function(department) {

    // User
    return { 
        name : "홍길동입니다.",
        age : 10,
        userDept : department
    };
   
});