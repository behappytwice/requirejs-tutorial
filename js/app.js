requirejs.config({
    // 모듈을 로딩하기 위한 루트 경로
    baseUrl: 'js/lib',
    //  app로 시작하는 모듈은
    //  baseUrl의 js/lib 디렉토리를 기준으로 상대적으로
    paths: {
        app: '../app'
    }
});
requirejs([],function() {
    document.getElementById('content').innerHTML = "안녕하세요";
});
