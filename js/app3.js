

requirejs.config({
    // 모듈을 로딩하기 위한 루트 경로
    baseUrl: 'js/lib',
    //  app로 시작하는 모듈은
    //  baseUrl의 js/lib 디렉토리를 기준으로 상대적으로
    paths: {
        app: '../app'
    }
});
requirejs(['jquery', 'app/sub'],
    function($, sub) {
        // jQuery, app/sub 모듈이 모두 로딩되고
        // 이곳에서 부터 사용할 수 있습니다.
        $("#content").html(sub);
    }
);
